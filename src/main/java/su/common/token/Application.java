package su.common.token;

import su.common.token.utils.TokenUtils;

/**
 * Created by vitaly_fomin on 20.11.17.
 */
public class Application {

	public static void main(String[] args) {
		if (args.length == 0) {
			System.err.println("Invalid secret key argument!");
		} else {
			String secret = args[0];
			String token = TokenUtils.generateToken(secret);
			System.out.println(token);
		}
	}
}
