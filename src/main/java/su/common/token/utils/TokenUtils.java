package su.common.token.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;

import java.io.UnsupportedEncodingException;

/**
 * Created by vitaly_fomin on 20.11.17.
 */
public class TokenUtils {
	private static final String BEARER = "Bearer ";

	private TokenUtils() {
		throw new IllegalStateException("Utility class");
	}

	public static String generateToken(String key) {
		try {
			Algorithm algorithm = Algorithm.HMAC256(key);
			String jwt = JWT.create()
					.sign(algorithm);
			return BEARER + jwt;
		} catch (UnsupportedEncodingException ignored) {
			return "";
		}
	}
}
